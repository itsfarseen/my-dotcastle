#!/bin/bash
source $DOTCASTLE/_bash_helpers/load_helpers
BUILDDIR='./build/home'
mkdir -p $BUILDDIR

BUILDFILE="$BUILDDIR/tmux"

echo >$BUILDFILE
for i in ./home.include/*.tmux; do
  cat "$i"|envsubst_safe|prepend_header '# --- '$i' --- '  >>"$BUILDFILE"
done
