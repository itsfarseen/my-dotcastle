#!/bin/bash

source $DOTCASTLE/_bash_helpers/load_helpers

BUILDFILE='./build/home/.profile'

mkdir -p $(dirname $BUILDFILE)

echo >$BUILDFILE

env_vars_file="$DOTCASTLE/common/environment_vars"
cat "$env_vars_file"|prepend_header '# --- '$env_vars_file' --- '  >>"$BUILDFILE"

for a in $DOTCASTLE/common/*.aliases; do
    cat "$a"|prepend_header '# --- '$a' --- '  >>"$BUILDFILE"
done

cat ./ssh_keychain.home >> $BUILDFILE
cat ./capslock_to_backspace.home >> $BUILDFILE
