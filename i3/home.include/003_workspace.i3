
# switch to workspace
bindsym $mod+1 workspace number 1: Web
bindsym $mod+2 workspace number 2: Code
bindsym $mod+3 workspace number 3: Code
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7: Docs
bindsym $mod+8 workspace number 8: Gitter
bindsym $mod+9 workspace number 9: Notes
bindsym $mod+0 workspace number 10: Temp

# move focused container to workspace number and switch to workspace
bindsym $mod+Shift+1 move container to workspace number 1: Web;     workspace number 1: Web
bindsym $mod+Shift+2 move container to workspace number 2: Code;    workspace number 2: Code
bindsym $mod+Shift+3 move container to workspace number 3: Code;    workspace number 3: Code
bindsym $mod+Shift+4 move container to workspace number 4;          workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5;          workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6;          workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7: Docs;    workspace number 7: Docs
bindsym $mod+Shift+8 move container to workspace number 8: Gitter;  workspace number 8: Gitter
bindsym $mod+Shift+9 move container to workspace number 9: Notes;   workspace number 9: Notes
bindsym $mod+Shift+0 move container to workspace number 10:Temp;    workspace number 10: Temp

# move focused container to workspace
bindsym $mod+Ctrl+1 move container to workspace number 1: Web
bindsym $mod+Ctrl+2 move container to workspace number 2: Code
bindsym $mod+Ctrl+3 move container to workspace number 3: Code
bindsym $mod+Ctrl+4 move container to workspace number 4
bindsym $mod+Ctrl+5 move container to workspace number 5
bindsym $mod+Ctrl+6 move container to workspace number 6
bindsym $mod+Ctrl+7 move container to workspace number 7: Docs
bindsym $mod+Ctrl+8 move container to workspace number 8: Gitter
bindsym $mod+Ctrl+9 move container to workspace number 9: Notes
bindsym $mod+Ctrl+0 move container to workspace number 10:Temp

bindsym $mod+z workspace back_and_forth
bindsym $mod+Shift+z move container to workspace back_and_forth; workspace back_and_forth
